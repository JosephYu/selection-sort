"""
Instruction: write a function that sorts a list of numbers in ascending order.

Selection sort is a sorting algorithm. It sorts a list by selecting the smallest value that is on the right side of the invisible hand and switching it with the value that is at the close right of the hand.
The invisble hand starts in front of index 0 - the invisible hand is marked as "|" --> [| 10, 12, -1, 100, 4]
In other words selecting the minimum value on the right side and switching it with the element directly right of the hand.

lst = [| 10, 12, -1, 100, 4] --> original list, minimum on the right side of the hand: -1 at index 2, element directly right of the hand: 10 at index 0, perform the switch.
lst = [-1, | 12, 10, 100, 4] --> 1st iteration, minimum on the right side of the hand: 4 at index 4, element directly right of the hand: 12 at index 1, perform the switch.
lst = [-1, 4, | 10, 100, 12] --> 2nd iteration, minimum on the right side of the hand: 10 at index 2, element directly right of the hand: 10 at index 2, perform the switch.
lst = [-1, 4, 10, | 100, 12] --> 3rd iteration, minimum on the right side of the hand: 12 at index 4, element directly right of the hand: 100 at index 3, perform the switch.
lst = [-1, 4, 10, 12, | 100] --> 4th iteration, stop when there is only 1 value on the right side of the hand because it will be the biggest value in the whole list.
"""

def selection_sort(lst):

    """
    Input: A list of unsorted integers.
    This algorithm modifies and sorts the input list in ascending order.
    """

    for i in range(len(lst)-1): # stop before the last value because it will already be the biggest value in the whole list and won't need to be switched with any other elements.
        min_i = lst.index(min(lst[i::])) # min_i will contain the index of the minimum value on the right side of the hand
        lst[i], lst[min_i] = lst[min_i], lst[i] # perform the switch with the close right value of the hand
        # print(lst) # uncomment to see each iteration

lst = [10, 12, -1, 100, 4]
print("unsorted list: " + str(lst))
selection_sort(lst)
print("sorted list: " + str(lst))